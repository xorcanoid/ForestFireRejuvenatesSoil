﻿using RimWorld;
using System.Collections.Generic;
using UnityEngine;
using Verse;

namespace ForestFireRejuvenatesSoil
{
    public class Settings : ModSettings
    {
        public static float fertility_amount_inc = 0.5f;
        
        public void DoWindowContents(Rect inRect)
        {
            Listing_Standard list = new Listing_Standard(GameFont.Small);
            list.Begin(inRect);
            list.TextEntry("ForestFireRejuvenatesSoil.desc".Translate());
            list.SliderLabeled("ForestFireRejuvenatesSoil.fertility_amount_inc".Translate(), ref fertility_amount_inc, "{0:n2}", 0f, 2f, null);
            list.End();

        }
        public override void ExposeData()
        {
            base.ExposeData();
            Scribe_Values.Look(ref fertility_amount_inc, "fertility_amount_inc", 0.5f);
        }
    }

    // Token: 0x02000002 RID: 2
    public static class Listing_StandardExtensions
    {
        // Token: 0x06000001 RID: 1 RVA: 0x00002050 File Offset: 0x00000250
        public static void SliderLabeled(this Listing_Standard ls, string label, ref int val, string format, float min = 0f, float max = 100f, string tooltip = null)
        {
            float num = (float)val;
            ls.SliderLabeled(label, ref num, format, min, max, null);
            val = (int)num;
        }

        // Token: 0x06000002 RID: 2 RVA: 0x00002074 File Offset: 0x00000274
        public static void SliderLabeled(this Listing_Standard ls, string label, ref float val, string format, float min = 0f, float max = 1f, string tooltip = null)
        {
            Rect rect = ls.GetRect(Text.LineHeight);
            Rect rect2 = rect.LeftPart(0.7f).Rounded();
            Rect rect3 = rect.RightPart(0.3f).Rounded().LeftPart(0.67f).Rounded();
            Rect rect4 = rect.RightPart(0.1f).Rounded();
            TextAnchor anchor = Text.Anchor;
            Text.Anchor = TextAnchor.MiddleLeft;
            Widgets.Label(rect2, label);
            float num = Widgets.HorizontalSlider(rect3, val, min, max, true, null, null, null, -1f);
            val = num;
            Text.Anchor = TextAnchor.MiddleRight;
            Widgets.Label(rect4, string.Format(format, val));
            if (!tooltip.NullOrEmpty())
            {
                TooltipHandler.TipRegion(rect, tooltip);
            }
            Text.Anchor = anchor;
            ls.Gap(ls.verticalSpacing);
        }
    }
}
