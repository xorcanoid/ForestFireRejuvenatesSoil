﻿using System.Reflection;
using Harmony;
using RimWorld;
using static RimWorld.ThingDefOf;
using Verse;
using UnityEngine;

namespace ForestFireRejuvenatesSoil
{
    public class ForestFireRejuvenatesSoil : Mod
    {
        public static Settings Settings;

        public ForestFireRejuvenatesSoil(ModContentPack content) : base(content)
        {
            Settings = GetSettings<Settings>();
            HarmonyInstance.Create("ForestFireRejuvenatesSoil").PatchAll(Assembly.GetExecutingAssembly());
        }

        public override void DoSettingsWindowContents(Rect inRect)
        {
            Settings.DoWindowContents(inRect);
        }

        public override string SettingsCategory()
        {
            return "ForestFireRejuvenatesSoil".Translate();
        }
    }

    [HarmonyPatch(typeof(Filth))]
    [HarmonyPatch("ThinFilth")]
    class ThinFilth_Patch
    {
        static void Prefix(Filth __instance)
        {
            if (__instance.def != FilthAsh) return;
            Map map = __instance.Map;

            float maxfertility = 0;
            foreach (TerrainThreshold thresh in map.Biome.terrainsByFertility)
            {
                TerrainDef type = thresh.terrain;
                maxfertility = thresh.max;
            }

            IntVec3 position = __instance.Position;
            TerrainDef terrain = map.terrainGrid.TerrainAt(position);
            float newfertility = terrain.fertility + Settings.fertility_amount_inc;
            if (newfertility > maxfertility) newfertility = maxfertility;

            TerrainDef newTerrain = TerrainThreshold.TerrainAtValue(map.Biome.terrainsByFertility, newfertility);
            map.terrainGrid.SetTerrain(position, newTerrain);

            if (__instance.thickness - 1 == 0)
            {
                // Gonna disappear after this.
            }
        }
    }

}
/*
.method public hidebysig
    instance void ThinFilth() cil managed
{
	// Method begins at RVA 0xfc698
	// Code size 55 (0x37)
	.maxstack 8

	IL_0000: ldarg.0
	IL_0001: dup
   IL_0002: ldfld int32 RimWorld.Filth::thickness
   IL_0007: ldc.i4.1
	IL_0008: sub
   IL_0009: stfld int32 RimWorld.Filth::thickness
   IL_000e: ldarg.0
	IL_000f: call instance bool Verse.Thing::get_Spawned()
	IL_0014: brfalse IL_0036

   IL_0019: ldarg.0
	IL_001a: ldfld int32 RimWorld.Filth::thickness
   IL_001f: brtrue IL_0030

   IL_0024: ldarg.0
	IL_0025: ldc.i4.0
	IL_0026: callvirt instance void Verse.Thing::Destroy(valuetype Verse.DestroyMode)
	IL_002b: br IL_0036

   IL_0030: ldarg.0
	IL_0031: call instance void RimWorld.Filth::UpdateMesh()

	IL_0036: ret
} // end of method Filth::ThinFilth

    // RimWorld.Filth
public void ThinFilth()
{
	this.thickness--;
	if (base.Spawned)
	{
		if (this.thickness == 0)
		{
			this.Destroy(DestroyMode.Vanish);
		}
		else
		{
			this.UpdateMesh();
		}
	}
}
